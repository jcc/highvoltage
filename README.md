# highvoltage's roadmap for bullseye

## General development

 - [ ] Debian options screen for calamares (sources.list options, participate in popcon, etc)
 - [ ] Make md5sumchecker tool that we use on AIMS Desktop more generic, release as upstream project and consider adding to Debian media

## WNPP

 - [x] notepadqq (current packaging is good, need to weed out embedded fonts)
 - [~] foliate - https://robbinespu.gitlab.io/blog/2019/10/18/foliate-a-simple-and-modern-ebook-viewer-for-linux/

## debian-installer

 - [ ] make remove packages list not hard coded (#655198)
 - [ ] use grub for legacy boot too

## debian-live

- [ ] xfce settings (discussion phase)
- [ ] Cinnamon driver warning
- [ ] calamares mirror config
- [ ] calamares popcon screen
- [ ] calamares package copy blacklist
- [ ] Calamares slideshow
- [ ] calamares raid support

# highvoltage's roadmap for buster

## Debian Desktop

- [x] Finalize process for artwork selection https://wiki.debian.org/DebianDesktop/Artwork/Buster

## Python Team

- [x] Split of fabulous to 3 binary packages (python2, python3 and libfabulous)
- [x] Add python3 package for grapefruit (patch now available https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=903477)

## Debian Live

Find solutions to annoying desktop paper cuts

- [x] ibus popup (bug: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=905790)

## DebConf Video

- [x] Get TillyTally ready for PoC / integration with voctomix

## Informal WNPP

 - [x] zram-tools (basically needs a manpage)

## Misc

 - [x] Propose a new favicon for salsa. GitLab /does/ now support it!!!
